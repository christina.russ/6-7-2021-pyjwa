# We'll need import unittest
import unittest

# Someone has pointed out that we need to mock the DAO in this case as a function call to get_all_users within the
# service layer triggers yet another call to the DAO layer. This makes the user_dao a dependency of the user_service.
# If we want to prevent this call to the DAO layer, we need to mock our user_dao. We can do so by using using unittest's
# built-in mock functionality.

from unittest.mock import Mock

# Since we wish to mock the user_dao, let's import it
import src.dao.user_dao as ud

# We also need to import our object under test
import src.service.user_service as us

# Importing the user model because we want to create dummy users
import src.models.user as user

# Let's set up our test class

class UserServiceTest(unittest.TestCase):

    # What should the setup look like for this test case? We have voted to create some mock data in our setup to avoid
    # using real data
    def setUp(self):
        # We need to create a mock so that when get_all_users is called, mock values (stubs) are returned instead.
        ud.get_all_users = Mock(return_value=[(1, 'Jim', 'the Gemchanter', 'jim@gem.com', list())])


    # We will test our service function for getting all users.
    # As a general note, we should not be writing tests that pull data from our production database.
    # This also isn't a unit test (even if we were using dummy data) simply because we can be more granular. Right now,
    # if this test were to fail, we would not know if it was because of a bug in our service layer or user_dao (repository
    # layer).

    # We've since modified this so that we are not hitting our DB. Before we mocked, this was an integration test. It is
    # now a true unit test as we've mocked out our dependencies.
    def test_get_all_users(self):
        # What about the return value for get_all_users would we like to test?l In other words, what are our assertions?
        self.assertIsInstance(us.get_all_users(), dict)
        self.assertGreater(len(us.get_all_users()), 0)
        self.assertEquals(us.get_all_users()[1]._user_name, 'Jim the Gemchanter')