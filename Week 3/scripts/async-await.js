// The async keyword is added to a functions to tell them to return a promise rather than a value. The await keyword can only be used within an async function.

xhr_promise = new Promise((resolve, reject) => {
    let xhr = new XMLHttpRequest()
    let url = 'http://hp-api.herokuapp.com/api/characters'

    xhr.onreadystatechange = async function () {

        if (xhr.readyState === 4 && xhr.status === 200) {
            resolve(JSON.parse(xhr.response))
        } else if (xhr.readyState === 4 && xhr.status != 200) {
            reject('characters not retrieved')
        }
    }
    // Make sure that you don't accidentally put your open and send functions inside of the callback function as then the readyState will never change.
    xhr.open('GET', url)
    xhr.send()
})

async function manipulateDom() {
    let dropdown = document.getElementById('favorite_character')

    // Refactored for practicality as we now await the value of this promise
    let characters = await xhr_promise

    for (let character of characters) {
        let option = document.createElement('option')
        option.innerText = character['name']
        dropdown.append(option)
    }
}

window.onload = function(){
    this.manipulateDom()
}