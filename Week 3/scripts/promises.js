// A "promise" is the representation of the eventual completion ( or failure) of an async operation and its resulting value. It is essentially a proxy for a value that is not necessarily know when the promise is created. It allows us to associate handlers with async actions' eventual success or failure.

// Creation of a promise:

let a_promise = new Promise((resolve, reject) => {
    resolve('not HP characters')
})

a_promise.then(
    // With promises, we can only access the returned value from within the context of our promise (e.g. within this .then function)
    (data) => {
        console.log('promise has been resolved'); 
        console.log(data)
    }
)

// ================================================================

let xhr_promise = new Promise((resolve, reject) => {
    let xhr = new XMLHttpRequest()
    let url = 'http://hp-api.herokuapp.com/api/characters'

    xhr.onreadystatechange = function(){

        if(xhr.readyState === 4 && xhr.status === 200){
            let characters = resolve(JSON.parse(xhr.response))

            console.log(characters) //definitely not the characters you want
           
        } else if(xhr.readyState === 4 && xhr.status != 200){
            reject('characters not retrieved')
        }
    }

    xhr.open('GET', url)
    xhr.send()
})

xhr_promise.then(characters => {
    console.log(characters)
    let dropdown = document.getElementById('favorite_character')

    for(let character of characters){
        let option = document.createElement('option')
        option.innerText = character['name']
        dropdown.append(option)
    }
})