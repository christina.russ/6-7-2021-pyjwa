// We will be using a technology called AJAX in order to make HTTP requests to the Harry Potter API. AJAX stands for "Asynchronous JavaScript and XML". We should that the most notable feature of AJAX is its ability to make HTTP requests asynchronously. When we make a request asynchronously, we do not have to wait for a response from the server in order for our scripts to continue executing.

// The primary type used with AJAX is called the "XMLHttpRequest". The XMLHttpRequest has various "ready states" which denote which stage the request is in.

let xhr = new XMLHttpRequest() // ready state 0
let url = 'http://hp-api.herokuapp.com/api/characters'

// Any time the readyState of the XHR changes, this callback function will be invoked.
xhr.onreadystatechange = function(){
    // A ready state of 3 indicates that your HTTP request has made it to the server and is being processed and a ready state of 4 indicates that the server is sending back a response.

    if(xhr.readyState === 4 && xhr.status === 200){
        // Only if the readyState is 4 AND the HTTP response status code is 200 do we want to perform DOM manipulation.

        // We want to access the response body. Please note that the response body is JSON and the we want to convert it into a JS object for use here:
        let characters = JSON.parse(xhr.response)

        // We've decided to add the character names as options to our existing select element. As such, we need to select that element for manipulation.

        let dropdown = document.getElementById('favorite_character')

        for(let character of characters){
            // Note that we need to iterate over the characters in the response as we want to add one option per character.
            let option = document.createElement('option')
            option.innerText = character['name']
            dropdown.append(option)
        }
    }
}

xhr.open('GET', url) // ready state 1
xhr.send() // ready state 2