// The fetch API provides a simple way of fetching resources asynchronously.

//Commented out for your reference.

// function get_characters() {
//     let url = 'http://hp-api.herokuapp.com/api/characters'

//     fetch(url)
//         .then(response => {
//             response.json()
//                 .then(json_body => {

//                     let dropdown = document.getElementById('favorite_character')

//                     for (let character of json_body) {
//                         let option = document.createElement('option')
//                         option.innerText = character['name']
//                         dropdown.append(option)
//                     }
//                 })
//         })
// }

async function get_characters() {
    let url = 'http://hp-api.herokuapp.com/api/characters'
    let response = await fetch(url)
    // Remember that fetch returns a response object, not the direct response body
    let characters = await response.json()
    let dropdown = document.getElementById('favorite_character')

    for (let character of characters) {
        let option = document.createElement('option')
        option.innerText = character['name']
        dropdown.append(option)
    }
}

// The window object has an onload property that we can use to perform operations as soon as the window loads:

// Please note that for immediately firing off functions, you can use IIFE (Immediately Invoked Function Expressions). IIFEs, which are anon functions, also help you avoid cluttering the global namespace.
window.onload = function () {
    this.get_characters()
}