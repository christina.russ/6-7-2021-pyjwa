// The alert function pops up those annoying 2000s alert boxes. These are not considered user-friendly.
// window.alert("You've won a free owl.")
// window.alert("Your head is now on fire.")
// window.alert("Please install Flash for this website.")

//Let's do some basic DOM manipulation. Recall that our DOM is our Document Object Model, which provides a representation of all of the web elements on our page.

// We'll first need to select an element for manipulation.

let boot = document.querySelector('#hat > img')

// We want to be able to change the color of the image's border, but in response to user interactions. As such, we'll need to use an event listener. Event listeners are added to elements. We pass callback functions which should be invoked in response to an interaction on the frontend to the addEventListener function.

// We have decided that we want to keep a list of colors to iterate over and the index of the color that is currently displayed outside of the scope of that function.

const colors = ['red', 'green', 'steelblue']
// Everyone wanted the one true iteration variable "i"...
let i = 2

boot.addEventListener('click', () => {
    i = (i + 1)%3
    boot.style.borderColor=colors[i]
})