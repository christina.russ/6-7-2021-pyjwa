// The describe function in Jasmine denotes a test suite. This is a collection of tests. Note that Jasmine is a BDD (Behavior-Driven Development) testing framework, which means that the functionality we are testing is specified at a high level. This is usually specified at such a high level that even nontechnical members (e.g. a business analyst) of your team could identify what behavior is being tested.

// NOTE: You should place an "x" before "describe" or "it" to disable either the entire suite or a particular test.
xdescribe('Calculator Tests', () => {

    // Can't test the calculator without having a Calculator object. (e.g. object under test)
    let calculator;

    // Of course, you can set up and tear down your test bed as usually by using before and after hooks like so:
    
    beforeEach(() => {
        // Set up the test bed.
        calculator = new Calculator()
    })

    // An actual test is denoted by the "it" function. When using this function, we specify what we are testing at a high level and pass a callback function with out test logic to "it".
    it('should properly add two numbers together', () => {
        // In Jasmine, we have "expectations" rather than "assertions". We are using expectations to verify that the expected outcome matches the actual outcome.
        expect(calculator.add(5, 5)).toEqual(10)
    })

    it('should properly subtract two numbers', () => {
        expect(calculator.subtract(5, 5)).toEqual(0)
    })

    afterEach(() => {
        // Tear down the test bed.
    })
})