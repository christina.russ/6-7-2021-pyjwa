Feature: Log in to Swag Labs

Background:
	Given a user is on the login page

Scenario: A user enters the incorrect credentials
	When a user enters the incorrect email and incorrect password
	Then a user is not redirected to the inventory page

Scenario: A user enters the correct credentials
	When a user enters the correct email and correct password
	Then a user is redirected to the inventory page