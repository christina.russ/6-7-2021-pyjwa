package com.poms;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/*
 * This is our page object model for the Swag Labs
 * login page. Note that it is no different from POMs
 * you've seen in other languages. It is just a repository
 * of the web elements on a page and some convenience methods
 * for interacting with those elements.
 */
public class SwagLabsLoginPage {
	
	@FindBy(id = "user-name")
	public WebElement usernameBox;
	@FindBy(id = "password")
	public WebElement passwordBox;
	@FindBy(id = "login-button")
	public WebElement loginButton;
	
	/*
	 * As usual, we'll be passing our WebDriver to our page object model because we
	 * don't need multiple instances of the web driver.
	 */
	public SwagLabsLoginPage(WebDriver driver) {
		/*
		 * PageFactory makes using the POM design pattern easier. It removes the need
		 * for the developer to manually grab web elements by giving us access to the
		 * @FindBy annotation, which simply requires that you pass the Selenium locator
		 * to PageFactory.
		 */
		PageFactory.initElements(driver, this);
	}
	
	/*
	 * Define your convenience methods:
	 */
	
	public void login(String username, String password) {
		this.usernameBox.sendKeys(username);
		this.passwordBox.sendKeys(password);
		this.passwordBox.sendKeys(Keys.ENTER);
	}
}
