package com.cucumber;

import java.time.Duration;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.poms.SwagLabsLoginPage;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

/*
 * I'm placing my glue code inside of this file.
 * Recall that our glue code is just the implementation
 * of our feature file (e.g. our Cucumber steps).
 */
public class SwagLabsLogin {

	/*
	 * We need to do some simple setup of our webdriver as I'm sure you remember
	 * that we need the driver to automate our browser.
	 * 
	 * Please note that Cucumber has its own annotations for setup. Do not mix
	 * Cucumber with setup and teardown annotations from another framework.
	 */

	WebDriver driver;

	@Before
	public void setup() {
		// We'll use the System class to set a System property
		// which specifies the location of our driver (Chrome driver in my case).
		System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");
		// Please select the proper implementation for your webdriver.
		driver = new ChromeDriver();
		
		//Of course, we can still apply Selenium waits in Java as well.
		//Implicit wait.
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		
		//Explicitly wait
		WebDriverWait wait = new WebDriverWait(driver, 2000);
		wait.until(ExpectedConditions.elementToBeClickable(By.id("login-button")));
		
		//You also have access to a Fluent wait, which is an explicit, but it allows
		//for more custom polling options and is generally considered to be a "smarter"
		//wait than your standard explicit wait.
		FluentWait<WebDriver> fluentWait = new FluentWait<>(driver)
				.withTimeout(Duration.ofSeconds(3))
				.pollingEvery(Duration.ofMillis(500))
				.ignoring(NoSuchElementException.class);
		
		
		//Bonus: Switch to an alert that is popping up and dismiss it:
		//driver.switchTo().alert().dismiss();
		
		driver.get("https://www.saucedemo.com/");
	}

	@Given("a user is on the login page")
	public void a_user_is_on_the_login_page() {
		assert driver.getCurrentUrl().equals("https://www.saucedemo.com/");
	}

	@When("a user enters the incorrect email and incorrect password")
	public void a_user_enters_the_incorrect_email_and_incorrect_password() {
		SwagLabsLoginPage sllp = new SwagLabsLoginPage(driver);
		sllp.login("abc", "abc");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Then("a user is not redirected to the inventory page")
	public void a_user_is_not_redirected_to_the_inventory_page() {
		assert driver.getCurrentUrl().equals("https://www.saucedemo.com/");
	}

	@When("a user enters the correct email and correct password")
	public void a_user_enters_the_correct_email_and_correct_password() {
		SwagLabsLoginPage sllp = new SwagLabsLoginPage(driver);
		sllp.login("standard_user", "secret_sauce");
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Then("a user is redirected to the inventory page")
	public void a_user_is_redirected_to_the_inventory_page() {
		assert driver.getCurrentUrl().equals("https://www.saucedemo.com/inventory.html");
	}

	/*
	 * Of course, we need to do some teardown to close the browser.
	 */
	@After
	public void teardown() {
		this.driver.quit();
	}
}
