# This is called a "Step Definition File" as it details the source code for the tests we specified that
# we would write in our associated feature file. Also note that people also refer to this source code as
# "Glue Code".

from behave import *
from selenium_content.poms.swag_labs_login_page import SwagLabsLoginPage

@given('a user is on the home page of Swag Labs')
def test_on_home_page(context):
    context.driver.get('https://www.saucedemo.com/')

@given('a user enters the correct username and the correct password')
def test_user_enters_username(context):
    context.sll_page.enter_credentials()

@when('the user pushes the submit button')
def test_user_clicks_submit(context):
    context.sll_page.click_login_button()

# Even though we could not successfully specify our expected outcome using our background (at least in the
# way we tried), we found that we could just reuse the exact same text for the "Then" step in our feature
# file and get away with only having one step in our step definition file for this @then since both @then
# decorated functions made the same assertion anyway.

# @then('the user is redirected to the dashboard page')
# def user_is_redirected(context):
#     assert 'inventory' in context.driver.current_url

@when('the user hits the enter button')
def test_user_hits_enter(context):
    context.sll_page.hit_enter_button_to_login()

@then('the user is redirected to the dashboard page')
def test_user_redirected_after_using_enter(context):
    assert 'inventory' in context.driver.current_url