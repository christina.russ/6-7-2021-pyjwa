# This is a feature file. It is a cucumber-specific file that contains scenarios written in Gherkin.
  # Gherkin is a BDD language used to specify what we are testing at a high level. Note that Gherkin
  # has support in over 70 languages and has various keywords.

  # The Feature keyword denotes what feature we are testing. Note that a feature contains a collection of
  # scenarios.
  Feature: Swag Labs Login

    # A background is similar to a scenario in that it include steps. These steps, however, allow you to
    # add some shared context to all of your scenarios. In other words, the steps associated with a background
    # belong to all scenarios.
    Background: A user is on the home page of Swag Labs and is entering the correct username and password
      Given a user is on the home page of Swag Labs
      And a user enters the correct username and the correct password

    # The Scenario keyword denotes the use case scenario that is under test. A use case scenario details
    # a specific example of how an end user might engage with your application.
    # Please note that you can use custom tags for your scenarios and reference these tags later to logically
    # group these scenarios.
    @custom-tag @my-tag
    Scenario: A user is on the home page of Swag Labs and would like to login with correct credentials.
      # I can now detail the specific steps a user would take to actually log in using "Cucumber steps".
      # Given specifies a pre-condition (e.g. before the user has interacted the feature)
#      Given a user is on the home page of Swag Labs
#      # When specifies the action that the user would take.
#      When the user enters the correct username
#      # If you want to, you can break up your steps even further using the "and" and "but" keywords. Please
#      # note that these keywords (as far as Cucumber is concerned) do not do the same thing functionally but they
#      # just create more meaning for anyone who is reading this feature file.
#      When the user enters the correct password
      When the user pushes the submit button
      # Then specifies the expected result of the user's action
      Then the user is redirected to the dashboard page

    Scenario: A user is on the home page of Swag Labs and would like to login with correct credentials.
#      Given a user is on the home page of Swag Labs
#      When the user enters the correct username
#      And the user enters the correct password
      When the user hits the enter button
      Then the user is redirected to the dashboard page