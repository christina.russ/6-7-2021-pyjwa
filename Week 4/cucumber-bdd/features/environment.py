# You are allowed to provide a module called "environment.py" when using behave in order to configure
# your cucumber environment. Note that this will be referenced before any individual step definition file.
# We can behave global configuration here (e.g. modifying our behave context).

from selenium import webdriver
from selenium_content.poms.swag_labs_login_page import SwagLabsLoginPage

# before_all, as you might expect, runs before all of our Cucumber features
def before_all(context):
    # As such, this is a perfect place for us to add our webdriver to our behave context. Note that this
    # context gives you access to metadata about your features, scenarios, and steps - but that you should
    # treat this data as read-only. You can also add custom properties to your context.
    context.driver = webdriver.Chrome()
    context.sll_page = SwagLabsLoginPage(context.driver)

# Other options for setup and teardown:

def before_step(context, step):
    pass

def before_scenario(context, scenario):
    # We might want to get the url for the home page here because we have two scenarios that
    # involve logging in.
    pass

def before_feature(context, feature):
    pass

def before_tag(context, tag):
    pass

def after_tag(context, tag):
    pass

def after_feature(context, feature):
    pass

def after_scenario(context, scenario):
    pass

def after_step(context, step):
    pass

def after_all(context):
    # Remember to close the browser
    context.driver.quit()