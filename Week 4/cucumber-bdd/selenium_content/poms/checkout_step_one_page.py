class CheckoutStepOne:
    # We need to provide our first name, last name, and zip code in order to confirm our order. We'll need
    # to locate these text boxes in order to type the information in. We've decided to use one example of
    # xpath (XML Path Language) just to see what it looks like. Note, however, that xpath is a last resort -
    # especially full xpath as it does not create the most stable scripts.

    first_name_box_xpath = '//*[@id="first-name"]'
    last_name_box_id = 'last-name'
    postal_code_box_id = 'postal-code'
    continue_button_id = 'continue'

    def __init__(self, driver):
        self.driver = driver

    def enter_personal_info(self):
        # We want to type into these input boxes and click the continue button
        self.driver.find_element_by_xpath(self.first_name_box_xpath).send_keys('ugh')
        self.driver.find_element_by_id(self.last_name_box_id).send_keys('ugh')
        self.driver.find_element_by_id(self.postal_code_box_id).send_keys('ugh')
        self.driver.find_element_by_id(self.continue_button_id).click()