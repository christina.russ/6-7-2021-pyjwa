class CheckoutStepTwo:

    finish_button_id = 'finish'

    def __init__(self, driver):
        self.driver = driver

    def click_finish(self):
        # Let's click the "finish" button because we are done ordering our swag.
        self.driver.find_element_by_id(self.finish_button_id).click()