# There is a way to simulate an end user clicking keys on the keyboard (e.g. hitting the ENTER
# key, the arrow keys, etc.). We can do so using the "Keys" class.
from selenium.webdriver.common.keys import Keys

# This is what we refer to as a "Page Object Model" - POM for short. A Page Object Model serves as a
# repository of locators for elements on a web page. It also often includes useful methods for interacting
# with said web elements. The POM design pattern is useful as it saves us time since we can easily reuse this
# repository to later grab web elements from this specific page. NOTE: It can be tedious to make a POM for each
# view in a UI.

class SwagLabsLoginPage:
    # Let's define the elements that should exist in this repository. Note that I'm only using the locators
    # here. We're lazily grabbing the web elements since we might not necessarily ever need to grab a specific
    # element from this web page every time we interact with it.
    username_box_id = 'user-name'
    password_box_id = 'password'
    login_button_id = 'login-button'

    # It is very common to see the driver passed to each page object model via its constructor.
    def __init__(self, driver):
        self.driver = driver

    def enter_credentials(self):
        # This is the basic selection of a web element by its ID. Note that ID is one of the 8 basic "Selenium
        # locators". Please note that if you attempt to locate an element using an invalid ID, css-selector, etc.,
        # a NoSuchElementException will be raised. Also note that sometimes we get a NoSuchElementException when
        # an element can be selected in such a way but has not yet been rendered.
        self.driver.find_element_by_id(self.username_box_id).send_keys('standard_user')
        self.driver.find_element_by_id(self.password_box_id).send_keys('secret_sauce')

    def click_login_button(self):
        self.driver.find_element_by_id(self.login_button_id).click()

    def hit_enter_button_to_login(self):
        self.driver.find_element_by_id(self.password_box_id).send_keys(Keys.ENTER)

        # We've commented this code out because when we ported it over for our POM, we decided to just
        # immediately interact with the elements we had located. This is just here for your reference.

        # Now that we've located all of the elements we need, what do we want to do with them? We want to type in
        # our username and password then click the login button. Please note that below we have hardcoded the username
        # and password into the code. Note also, however, that we should not do this in automated scripts which are
        # using credentials we actually want to protect. As such, use environment variables, properties files, or any
        # other solution that keeps your credentials safe. Do NOT expose them here.

        # username_box.send_keys('standard_user')
        # password_box.send_keys('secret_sauce')
        # login_button.click()