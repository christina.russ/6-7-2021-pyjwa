class Cart:

    checkout_button_class_name = 'checkout_button'

    def __init__(self, driver):
        self.driver = driver

    def click_checkout_button(self):
        # Now we're ready to checkout, so let's hit the checkout button. We've decided to locate this button by its
        # class just to see more Selenium locators.
        self.driver.find_element_by_class_name(self.checkout_button_class_name).click()