class InventoryPage:

    # The locators for the web elements we want to interact with:
    backpack_button_id = 'add-to-cart-sauce-labs-backpack'
    cart_link_css_selector = '#shopping_cart_container > a'

    def __init__(self, driver):
        self.driver = driver

    # We'll have 2 methods: one for adding an item to our cart, and another for viewing the cart

    # Adding an item (the backpack) to the cart

    def add_backpack_to_cart(self):
        # Now we wish to purchase the Sauce Labs Backpack.
        self.driver.find_element_by_id(self.backpack_button_id).click()

    def go_cart(self):
        # In order to view our cart, we cannot use an ID to click the link as the link does not have one. We have
        # decided to use a CSS selector to locate the element instead.
        self.driver.find_element_by_css_selector(self.cart_link_css_selector).click()