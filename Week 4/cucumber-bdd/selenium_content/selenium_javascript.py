from selenium import webdriver
import time

# As I said, you can execute JS in your browser using Selenium.

# But first, we need to navigate to the home page of Swag Labs:

driver = webdriver.Chrome()
driver.get('https://www.saucedemo.com/')

# Jonathan wants to remove the image of the Swag Labs mascot by using JavaScript to perform DOM
# manipulation.

# In order to execute JavaScript, we need to use the execute_script function.

javascript = "let outerDiv = document.getElementsByClassName('login_wrapper-inner'); let bot = " \
             "document.getElementsByClassName('bot_column'); outerDiv[0].removeChild(bot[0]); "

time.sleep(3)

driver.execute_script(javascript)