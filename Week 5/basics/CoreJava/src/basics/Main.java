package basics;

// This is a comment

/*
 * This is how you do a block comment
 * */

/**
 * What is this?
 * 		This is JavaDocs
 * */
public class Main {
	
	/*
	 * You need the exact method signature below for the JVM
	 * to recognize that this is the beginning of the program
	 * */
	public static void main(String[] args) {
		
		/*
		 * Primitive types:
		 * 	- int
		 * 	- char
		 * 	- double
		 * 	- float
		 * 	- short
		 * 	- long
		 * 	- byte
		 * 	- boolean
		 * 
		 * 
		 * bitter black coffee sure is for long days
		 * 
		 * boolean(1 bit) 
		 * byte(1 byte) 2^7 -1
		 * char(2 bytes) 
		 * short(2bytes) 
		 * int (4bytes) 
		 * float(4bytes) 
		 * long(8bytes) 
		 * double(8bytes)
		 */
		
		//numbers
		byte b = 127; //initialization and declaring
		short s = 20;
		int i = 34;
		long l = 443;
		
		//boolean
		boolean bool = true;
		
		//decimal numbers
		double d = 3.14d;
		float f = 3.14f;
		
		//characters
		char c = 'a';
		
		
		//ARRAY 
		
		/*
		 * An array is a series of data entries 
		 * (of the same data type) sequential in memory
		 * 
		 * 	NOTE: you cannot change the size of an array
		 * 
		 * arrays start at index 0
		 */
		
		int[] arr = {1,2,3,4,5}; // this allocated 5 spots
		int[] arr2 = new int[99]; // this allocated 99 spots
		
		System.out.println(arr);
		System.out.println(arr[2]);
		
		System.out.println(arr2[2]);
		
		/*
		 * Java has default values for instance variables as well as array initialization
		 * 	- default values:
		 * 		- int: 0
		 * 		- double: 0.0D
		 * 		- float: 0.0F
		 * 		- short: 0
		 * 		- long: 0L
		 * 		- boolean: false;
		 * 		- byte: 0
		 * 		- char: "empty character"
		 * 
		 * */
		
		
		/*
		 * What is Control Flow?
		 * 	Control is tools that we can utilize 
		 * 		to manipulate when lines of code gets executed
		 * 	- if else
		 * 	- while loops, do while loops
		 * 	- for loops / enhanced for loop
		 * 	- switch
		 * */
		
		
		/*
		 * if statements
		 */
		int cflow = 40;
		if(cflow > 50) {//condition
			
			//code to execute if condition is true
			System.out.println("Inside the if statement!");
		}else if(cflow > 30){
			//this will run if the if is false and the above condition is true
			System.out.println("inside the else if");
		}else {
			// do this if everything is incorrect
			System.out.println("in the else");
		}
		
		
		/*
		 * for loops
		 * */
		int[] forExample = {1,2,3,4,5};
		
		// this will iterate through the entire array
		System.out.println("FOR LOOP");
		for(int forVal = 0; forVal < forExample.length; forVal++) {
			System.out.print(forExample[forVal]);
		}
		
		//enhanced for loop
		System.out.println("\nENHANCED FOR LOOP");
		for(int x : forExample) {
			System.out.print(x);
		}
		
		
		/*
		 * Switch statements attempts to match some variable with the value it contains
		 * 	primitives that are allowed with switch statements:
		 * 	- byte
		 * 	- short
		 * 	- char 
		 * 	- int
		 * 
		 * 	- (the respective wrapper classes)
		 * 	- String
		 */
		
		int switchTest = 10;
		
		switch(switchTest) {
			case 5:
				System.out.println("the value is 5!");
				break;
			case 10: 
				System.out.println("the value is 10");
				break; //if you dont add a break it will print the default as well.
			default:
				System.out.println("Not 5 or 10 :(");
		}
		
		
		
	}
	
}