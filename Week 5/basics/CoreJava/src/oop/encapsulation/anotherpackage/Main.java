package oop.encapsulation.anotherpackage;

import oop.encapsulation.User;

public class Main {
	public static void main(String[] args) {
		//demonstrating that only public variables and methods can be viewed here
		User user = new User();
	}
}
