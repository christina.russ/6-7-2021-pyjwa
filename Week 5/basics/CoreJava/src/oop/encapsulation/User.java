package oop.encapsulation;

/*
 * Encapsulation allows us to protect or fields and methods with access modifiers
 * We have 4 access modifiers that we can use:
 * - public: any class in any package can use it
 * - protected: any class in any package can use it IF it is a child class (the kids can use it). 
 * 		OR if it is in the same package
 * - default: only the same package
 * - private: only the same class
 * 
 * 
 *                              public | protected | default | private
			  own class     |    X     |      X    |    X    |    X
              same package  |    X     |      X    |    X    |
              sub class     |    X     |      X    |         |
              any package   |    X     |           |         |
 * 
 */

// this class is commonly called (POJO) Plain Old Java Object
public class User {
	
	//generally you make the variables private and then you access them through getters and setters
	
	//having private variables that are accessed through getters and setters in one of the main ideas of encapsulation
	private int age;
	private String firstName;
	private String lastName;
	
	
	//getter
	public int getAge() {
		return this.age;
	}
	
	//setter
	public void setAge(int age) {
		if(age < 1) {
			return;
		}
		
		this.age = age;
	}
	

}
