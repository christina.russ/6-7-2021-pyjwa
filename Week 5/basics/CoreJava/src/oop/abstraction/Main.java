package oop.abstraction;

public class Main {
	
	public static void main(String[] args) {
		//you cannot instantiate a interface
		Truck truck = new ToyotaTundra();//What is this ? Upcasting
		
		truck.drive();
		
		Car car = new HondaCivic();
		car.drive();
	}
	
}
