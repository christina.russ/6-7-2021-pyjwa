package oop.abstraction;

public class HondaCivic extends Car {

	@Override // this is an annotation... it adds metadata to the method / class / variable
	void drive() {
		System.out.println("putputput vroom");
	}

}
