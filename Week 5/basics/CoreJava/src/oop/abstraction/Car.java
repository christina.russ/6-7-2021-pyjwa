package oop.abstraction;

public abstract class Car {
	int wheelCount;
	String model;
	String brand;
	
	//Can we have constructors?
	public Car() {
		//YEES XD
	}
	
	{
		//we can do these as well... no issues!
	}
	
	static {
		// no issues here =D
	}
	
	abstract void drive();
	
	
	void displayModelAndBrand() {
		System.out.println(this.brand + " " + this.model);
	}
	
}
