package oop.abstraction;


/*
 * An interface is a contract between the interface itself, and the class that implements it.
 */
public interface Truck {
	
	//all variables in an interface are public static final
	int wheelCount = 4;
	
	
	//Can I have a constructor?
	/*
	 * public Truck() {
	 * 
	 * }
	 * 
	 * no.... you cant
	 */
	
	/* can I have static / initializer blocks? */
	/*
	 * {
	 *  NOPE
	 * }
	 */
	
	/*
	 * static {
	 *  AND NOPE
	 * }
	 */
	
	
	//all methods in an interface are public and abstract
	//an abstract method does not have a body
	void drive();
	
	
	//unless if we use static
	static void honk() {
		System.out.println("HOOOOOOOOOOONNNNNNNNNNNNNKKKKKKKKKK");
	}
	
	
	//or we can use the default keyword (this was introduced in Java 8)
	default void honkLouder() {
		System.out.println("HHHHHHHHOOOOOOOOOOOOOOOOOOOOOOOOOOOOONNNNNNNNNNNNNNNNNNNNNNKKKKKKKKKK");
	}

}
