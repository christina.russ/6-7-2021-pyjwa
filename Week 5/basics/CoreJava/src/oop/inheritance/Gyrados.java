package oop.inheritance;

/*
 * This is going be our child class
 * */
public class Gyrados extends Pokemon{ 
	int health;
	/*
	 * you inherit from a parent using the extends keyword...
	 * Note: You can only extend (inherit) one class
	 * 
	 * 
	 * 
	 * 
	 * Lets talk about this and super
	 * 
	 * "this" is a keyword to reference entities within the class you are currently in.
	 * 	- it can be used for the current classes constructors, fields and methods
	 * 
	 * 
	 * "super" is a keyword to reference entities of the PARENT class of the class you are currently in
	 * */
	Gyrados(){
		//super is here implicitly
		this("test");
		System.out.println("child no arg constructor");
		this.health = 50; //this accesses the child variable health
		super.health = 100; //this access the parent variable health
		
		this.attack();
		super.attack("attacking......");
		

	}
	
	Gyrados(String test){
		//constructors implicitly call super();
		System.out.println("one arg constructor");
	}
	
	void attack() {
		System.out.println("I am attacking");
	}
	
}
