package oop.inheritance;

/*
 * This is going to be our parent class
 */

/*
 * Every class in java derives from the Object class
 * 
 * This is how the hierarchy looks
 * 	Object -> Pokemon -> Gyrados
 * 
 */
public class Pokemon {
	int id;
	int health;
	String name;
	String[] types;
	String[] abilities;
	
	Pokemon(){
		id = 0;
		name = "anonymous";
		types = new String[5];
		abilities = new String[5];
		health = 100;
		types[0] = "normal";
		abilities[0] = "splash";
		abilities[1] = "kick";
		
		System.out.println("Parent constructor called");
		
		Object obj = new Object();
		
	}
	
	void attack(String ability) {
		System.out.println(name + " is attacking with ability " + ability);
	}
}
