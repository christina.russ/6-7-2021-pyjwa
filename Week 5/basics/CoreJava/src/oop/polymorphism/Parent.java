package oop.polymorphism;

public class Parent {
	public int age;
	public String firstName;
	public String lastName;
	
	public Parent() {
		this.age = 48;
		this.firstName = "Old";
		this.lastName = "Fart";
	}
	
	public void eat() {
		System.out.println("Broccoli because Im a boring adult 0_0");
	}
	
	
	/*
	 * What is overloading?
	 * 
	 * Overloading is when you have the same method signature in the SAME class, but different parameters
	 * 
	 * NOTE: you do not have to have the same return type
	 * */
	
	public void drink() {
		System.out.println("Im am drink water because Im a boring adult 0__0");
	}
	
	public boolean drink(String liquid) {
		System.out.println("I am drinking " + liquid);
		return true; 
	}
	

}
