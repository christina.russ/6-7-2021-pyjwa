package oop.polymorphism;


/*
 * Overriding is done at runttime.
 * 
 * Overloading is done at compile time.
 * */
public class Main {
	public static void main(String[] args) {
		Parent parent = new Parent();
		
		parent.drink();
		parent.drink("monster");
		
		
		/*
		 * We have overridden the parent method so it will print out the child method
		 * */
		Parent human = new Child(); //this is upcasting
		human.eat();
		((Parent) human).eat(); // are we going to see the parent method?
		
		
		//are variables overridden? no. Variables are shadowed
		
		//When you shadow a variable... the child fields still exist, you just have to cast to get them.
		System.out.println(human.age);
		System.out.println(human.firstName);
		System.out.println(human.lastName);
		
		
		//when shadowing, you can still access the childs variables... in overriding you cant.
		System.out.println(((Child) human).age);
		
		//this will throw a ClassCastException
		//the compiler cant catch this, so overriding is done at runtime
		Parent classCastExample = new Parent();
		//Child child = ((Child) classCastExample);
		
		
		
		
		
	}
}
