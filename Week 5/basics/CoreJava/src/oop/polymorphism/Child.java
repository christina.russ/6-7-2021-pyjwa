package oop.polymorphism;

public class Child extends Parent{
	public int age;
	public String firstName;
	public String lastName;
	
	public Child() {
		//giving default values
		this.age = 12;
		this.firstName = "Pee";
		this.lastName = "Wee";
	}
	
	
	/*
	 * Overriding is when a child method has the same method name, parameters and returntype as the parent
	 */
	
	@Override
	public void eat() {
		System.out.println("Waffles allll the time because calories mean nothing to me! XD");
	}
	
	
}
