package exceptions;

public class BaDumTssException extends Exception{
	public BaDumTssException() {
		super();
	}
	
	public BaDumTssException(String message) {
		System.out.println(message);
	}
	
	public BaDumTssException(String message, Throwable clause) {
		super(message,clause);
	}
}
