package exceptions;

/*
 * The keywords for Exceptions:
 * 	- try, catch, finally
 *  - throws
 *  - throw
 * 
 * 
 * TAKEAWAYS
 * - Be able to tell the difference between an exception and an error,
 * - Be able to tell the difference between a checked and unchecked exception
 * 
 * - You can create your own exceptions XD
 * */
public class Main {
	public static void main(String[] args) throws BaDumTssException {

		//throwCheckedException();
		//throwUncheckedException();
		
		//apparently you can throw null XD
		throw null;
		
		//BaDumTssException bdts = new BaDumTssException("Another test");
		
//		throw new BaDumTssException("Brussel Sprouts are really gross",bdts);
		//throw new BaDumTssException("Brussel Sprouts are gross");
		
	}

	/*
	 * 
	 * Checked exceptions actually need to be handled utilizing try, catch, finally or throws.
	 * 
	 * The "throw" keyword allows us to actually throw an exception manually
	 * 
	 * The "try, catch" structure allows to handle exceptions.
	 * 
	 * 
	 * The "throws" keyword does not handle the exception, it passes it on to the
	 * next method
	 */
	public static void throwCheckedException() {

		//throw new Exception();
		 try { 
			 throw new Exception(); 
		 
		 }catch(Exception e) { 
			 e.printStackTrace(); 
		 }finally {
			 //the finally keyword always runs if something is caught or not
			 System.out.println("No matter what happens, the finally block will always run");
		 }
		 

	}
	
	
	/*
	 * Unchecked exceptions the compiler does not handle.
	 * 
	 * you actually need to fix your logic in the code
	 * */
	public static void throwUncheckedException() {
		
		
		throw new ArrayIndexOutOfBoundsException();
	}
}
