package collections;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.PriorityQueue;
import java.util.Queue;

public class QueueExample {

	/*
	 * Attributes of a Queue
	 * 		- First in First Out data structure (FIFO)
	 * 		- Example: actual queue from the DMV....
	 * 				When you get there, you have to wait for the people that got there first
	 * 
	 * Subclasses of a Queue
	 * 		- PriorityQueue
	 * 			- implements the queue data structure defined in the Queue interface
	 */
	public static void main(String[] args) {
		Queue<Integer> queue = new PriorityQueue<>();
		/*
		 * methods with the queue interface:
		 * 	- add(); adds to the end of the queue
		 * 	- peek(); returns head of queue but doesnt remove
		 * 	- poll(); returns head of queue and removes
		 */
		
		queue.add(1);
		queue.add(2);
		queue.add(3);
		
		//System.out.println(queue.peek());
		System.out.println(queue.poll());
		System.out.println(queue);
		
		for(Integer x : queue) {
			System.out.println(x);
		}
		
		Deque<Integer> deque = new ArrayDeque<>();
		
		/*
		 * deque.add(1); deque.addFirst(2); deque.add(3); deque.addFirst(4);
		 * 
		 * deque.
		 */

		Queue<Integer> anotherDeque = new ArrayDeque<>();
		//deque.
		//anotherDeque.
		//anotherDeque.
		
		System.out.println(deque);
		
	}
}
