package collections;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

public class ListExample {

	/*
	 * Attributes of a List:
	 * 		- stores an ordered collection of objects
	 * 		- can have duplicate values
	 * 
	 * Subclasses of a List:
	 * 		- ArrayList
	 * 			- non-synchronous
	 * 			- dynamic array
	 * 		- Vector
	 * 			- synchronous
	 * 			- dynamic array
	 * 		- LinkedList
	 * 			- non-synchronous
	 * 			- uses a doubly linked list
	 */
	
	
	public static void main(String[] args) {
		/*
		 * ArrayList utilizes arrays
		 */
		
		List<Integer> arrayList = new ArrayList<>();
		arrayList.add(1);
		arrayList.add(2);
		arrayList.add(3);
		arrayList.add(4);
		
		arrayList.remove(1);
		
		
		System.out.println(arrayList);
		System.out.println(arrayList.size());
		System.out.println(arrayList.isEmpty());
		
		
		List<Integer> vector = new Vector<>();
		vector.add(1);
		vector.add(2);
		vector.add(3);
		vector.add(4);
		
		vector.remove(2);
		
		System.out.println(vector.get(2));
		
		
		List<Integer> linkedList = new LinkedList<>();
		linkedList.add(1);
		linkedList.add(2);
		linkedList.add(3);
		linkedList.add(4);
		
		System.out.println(linkedList.get(2));
		
		
		
		
		
		/*
		 * Ways to iterate through a List
		 *  - Iterator 
		 *  - lambdas (forEach method)
		 *  - enhanced for loop
		 */
		
		//You can you use iterator to iterate through the arraylist
		Iterator<Integer> listIterator = arrayList.listIterator();
		
		
		arrayList.forEach(x -> System.out.println(x));
		/*
		 * while(listIterator.hasNext()) { System.out.println(listIterator.next()); }
		 * 
		 * //for each using lambdas arrayList.forEach(x -> System.out.println(x));
		 * 
		 * 
		 * //enhanced for loop for(Integer x : arrayList) { System.out.println(x); }
		 */
		
		
		
	}
}
