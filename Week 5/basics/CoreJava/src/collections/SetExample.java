package collections;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class SetExample {
/*
 * Attributes of a Set:
 * 	- stores an unordered set of elements
 * 	- it cannot have duplicate values
 * 	- can only store one null value
 * 
 * Subclasses of a Set
 * 		- HashSet
 * 			- utilizes a hashtable for storage
 * 		- TreeSet
 * 			- uses a binary search tree for storage
 * 			- the elements in a treeset are stored in ascending order
 * 
 * 
 * You would utilize a Set if you want to duplicate
 * 
 */
	public static void main(String[] args) {
		
		Set<String> hashSet = new HashSet<>();
		hashSet.add("Hot Dogs");
		hashSet.add("Burgers");
		hashSet.add("Sandwich");
		

		
		
		Set<String> treeSet = new TreeSet<>();
		treeSet.add("Hot Dogs");
		treeSet.add("Burgers");
		treeSet.add("Sandwich");
		
		treeSet.remove("Hot Dogs");
		
		
		System.out.println("\nHash Set\n");
		for(String i : hashSet) {
			System.out.println(i);
		}
		
		
		/*
		 * reading is efficient for treesets but writing is not
		 */
		System.out.println("\nTree Set\n");
		for(String i : treeSet) {
			System.out.println(i);
		}
		
		
		
		
	}
}
