package collections.notincollection;

import java.util.HashMap;
import java.util.Map;

/*
 * A Map in Java is a key value pair datatype. It is NOT apart of the Collections Interface
 * 
 * 	- this is equivalent to a dictionary in python
 */
public class MapExample {

	public static void main(String[] args) {
		Map<Integer,String> mapVar = new HashMap<>();
		mapVar.put(1, "cheese");
		mapVar.put(2, "grapes");
		mapVar.put(3, "oranges");
		
		System.out.println(mapVar.get(2));
		
		Map<Integer, User> users = new HashMap<>();
		User user1 = new User();
		
		users.put(1, user1);
		
		System.out.println(users.get(1));
	}
}

class User{
	String firstName = "Kevin";
	String lastName = "C";
	
	
	@Override
	public String toString() {
		return "User [firstName=" + firstName + ", lastName=" + lastName + "]";
	}
	
	

	
}

