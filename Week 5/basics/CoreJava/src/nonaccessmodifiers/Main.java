package nonaccessmodifiers;


/*
 * Non-Access modifier are keywords that can manipulate specific entities (class/method/variable) in java
 * - static
 * - final
 * - abstract
 * - synchronized (we wont be covering this)
 * 
 */


public class Main {
	public static void main(String[] args) {
		//System.out.println(StaticExample.staticVar);
		
		StaticExample.method();
		StaticExample.method();
		
		FinalExample finalExample = new FinalExample();
		//finalExample.x = 7; you cannot do this because the variable x is final
		
		
	}
}

class StaticExample{
	//the static keyword means the entity will be tied to the class... NOT the object
	// you can have static variables, methods and static blocks
	static int staticVar = 4;
	
	static void method() {
		System.out.println("Static Var val: " + staticVar);
		
		staticVar++;
		//staticVar += 1; // staticVar = staticVar + 1;
	}
	
	static {
		//once again this executes when the class is first referenced
		
		System.out.println("In the static block");
	}
}

//final classes cannot be inherited 
final class FinalExample{
	//final variables cannot be changed
	final int x = 4;
	
	//final methods cannot be overridden
	final void method() {
		System.out.println("I cannot be overridden");
	}
}
