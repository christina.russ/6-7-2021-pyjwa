package generics;


/*
 * Generics use angle brackets to create a placeholder for a future datatype
 */
public class Calculator {
	
	/*
	 * I want to implement a calculator....
	 */
	
	/*
	 * public Integer add(Integer x, Integer y) { return x + y; }
	 * 
	 * public Double add(Double x, Double y) { return x + y; }
	 */
	
	
	/*
	 * varargs utilizes the ... to specify that you dont know how many 
	 * arguments of this datatype is going to exist.
	 * 
	 * 	Something to note about var args... it can only be last argument in the parameter list 
	 * 		- varargs is essentially an array that we can loop through
	 * 		- this implies that you can only have varargs in a parameter list
	 */
	public <FluffyBunny extends Number, T extends Number> Double add(T arg1, FluffyBunny... x) {
		Double sum = arg1.doubleValue();
		for(FluffyBunny i : x) {
			sum += i.doubleValue();
		}
		return sum;
	}
	
	
	
	

}
