package generics;

public class Main {
	public static void main(String[] args) {
		Calculator calc = new Calculator();
		System.out.println(calc.add(5.43, 6.23, 12,43,23, 1337));
		System.out.println(calc.add(5.3, 65.4));
		System.out.println(calc.add(4.32));
		
		GenericClass<Integer> gClass = new GenericClass<>(42);
		
	}
}
