package generics;



/*
 * T can be replaced with any data type once it is instantiated
 * 
 */
public class GenericClass <T> {
	T val;
	
	public GenericClass(T val) {
		this.val = val;
	}
}
