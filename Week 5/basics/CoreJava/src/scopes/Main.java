package scopes;

public class Main {
	/*
	 * What is scope?
	 * 	variables have specific scope when they are declared in the program.
	 * 
	 * 	there are 4 different kinds of scope in java?
	 * 		- static scope  / class scope - cannot access any of the below scopes.
	 * 		- instance scope - can access static scope
	 * 		- method scope - can access instance / static scope
	 *      - block scope - can access method / instance / static scope     
	 *      
	 * */
	
	static int scope2 = 2; // this is static scope
	
	int scope1 = 1; // this is instance scope
	
	
	public static void main(String[] args) {
		
	}
	
	public void method() {
		int scope3 = 43; // this is method scope
		
		int x = scope2; //these can be accessed through method scope
		int y = scope1;
		
		if(true) {
			int blockScope = 20; // blockscope
			
		}
		
		
		//int methodScopeTest = blockScope; //this cant be achieved because method scope cannot reach block scope
	}
}
