package strings;


/*
 * What is a String?
 * 			its an array of characters. the string adds functionality to that array
 * 			- NOTE: String is not a primitive type. its an object
 * 
 * 		- a string is immutable (You cannot change the string)
 */
public class Main {
	public static void main(String[] args) {
		
		//ways of creating a string
		String str1 = new String("Hello Strings!");
		String str2 = "Hello String Literal!";
		
		/////////////////Common String Methods////////////////
		System.out.println(str1.length());// the length of the string
		System.out.println(str1.charAt(2));// gets the character at a specific index
		System.out.println(str1.substring(8)); //get everything from index 8
		System.out.println(str1.substring(8,10)); //get everything from index 8 to index 10 - 1
		System.out.println(str1.indexOf("Str")); //gets the starting index of the string
		System.out.println(str1.toUpperCase()); //makes the string uppercase
		System.out.println(str1.toLowerCase()); //makes the string lowercase
		
		System.out.println(str1.indexOf("x")); // this returns -1 if not found
		//System.out.println(str1.charAt(-1)); //StringIndexOutOfBounds
		
		
		System.out.println(str1);
		
		
		/////////////////////////String Buffer///////////////////////////////
	
		/*
		 * String buffer is mutable and IS synchronized (threadsafe)
		 * disadvantage:
		 * 	- slower than String Builder
		 */
		
		StringBuffer sbuff = new StringBuffer("In the end ");
		sbuff.append("it doesnt even matter");
		
		System.out.println(sbuff);
		
		
		////////////////////////////String Builder//////////////////////
		/*
		 * String Builder was introduced in JDK 1.5
		 * 
		 * String builder is mutable and is NOT synchronized (not threadsafe)
		 * 	- more efficient
		 */
		
		StringBuilder sb = new StringBuilder("Hello from ");
		sb.append("the other siiiiiiiiiiiiiiide!");
		
		System.out.println(sb);
		
		String s = "Kevin";
		
		String reverse = new StringBuilder(s).reverse().toString();
		
		System.out.println(reverse);
		
		

		
		
	}
}
