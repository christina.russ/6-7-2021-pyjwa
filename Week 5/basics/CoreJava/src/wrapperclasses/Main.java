package wrapperclasses;

public class Main {
	public static void main(String[] args) {
		/*
		 * Wrapper classes "wrap" around primitive data types
		 * 		- lets you treat primitives as objects
		 * 
		 */
		
		Boolean bool = true; //autoboxing
		//Boolean boo = new Boolean(true); this is deprecated (no longer supported)
		Boolean boo = Boolean.TRUE;
		Byte b = 10;
		Character c = 'a';
		Short s = 10;
		Integer i = 5;
		Integer intg = Integer.valueOf(1234); // can take a string and an int
		Integer intp = Integer.parseInt("1234"); // this only takes a string
		
		Float f = 121.2F;
		Long l = 1234L;
		Double d = 123.321;
		
		/*
		 * Autoboxing is the idea of having a primitive converted to wrapper automatically
		 */
		
		Integer autobox = 5; // autoboxing
		
		
		/*
		 * Unboxing is converting a wrapper class to a primitive
		 */
		
		int autoUnbox = autobox; //unboxing
		
		
	}

}
