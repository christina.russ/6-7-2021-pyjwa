package classesandobjects;

public class Main {
	public static void main(String[] args) {
		new Animal(); // this is instantiation
		
		Animal animal = new Animal("banana"); // this initialization and instantiation
		//animal.health;
		
		System.out.println(animal.heal(25));
	}
}
