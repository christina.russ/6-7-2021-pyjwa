
/*
 * packages are just folders of like classes / interfaces
 * */
package classesandobjects;

/*
 * class is a blueprint of how we create objects
 * 
 * What is an object?
 * 		an object is an instance of a class
 * 		- objects have state and behavior that is defined by the class
 * */
public class Animal {
	
	////////////////STATE (variables)//////////////////
	String name;
	int health = 50;
	
	
	////////////////Behavior (methods)/////////////////////////
	/*
	 * A method is reusable code that is called when ran
	 * 
	 * [return type] variableName (parameters){
	 * 		//logic goes here
	 * }
	 * 
	 * - return type: what data type you would like to return to who called it
	 * - parameters: variables that you can pass to the method to use
	 * */
	
	int heal(int amount) {
		health = health + amount;
		return health;
	}
	
	
	/*
	 * What is a constructor?
	 * 	its a method that is called when an object is instantiated
	 * 	 -if you dont explicitly make a constructor in the class, there is a default constructor.
	 *   -you can have multiple constructors
	 *   -constructors cannot have a return type
	 * */
	
	//this is a constructor
	Animal(){
		System.out.println("This is a constructor");
	}
	
	//this is constructor with one arg
	Animal(String word){
		System.out.println("Constructor with argument: " + word);
	}
	
	
	//What is this
	// this is an initializer block
	/*
	 * Its essential a method without a name. the block immediately invoked when object is instantiated
	 * */
	{
		System.out.println("INITIALIZER BLOCK");
	}
	
	//this is an static block is ran the first the class is loaded into memory
	static {
		System.out.println("Static block");
	}

}
