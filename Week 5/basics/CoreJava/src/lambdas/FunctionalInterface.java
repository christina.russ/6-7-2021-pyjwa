package lambdas;

//@FunctionalInterface
public interface FunctionalInterface {
/*
 * What is a functional interface?
 * 	A functional interface is just an interface that has exactly one abstract method
 */
	
	
	/*
	 * For this abstract method, I want to check if the string that is passed is even
	 */
	boolean isEven(String str);
}
