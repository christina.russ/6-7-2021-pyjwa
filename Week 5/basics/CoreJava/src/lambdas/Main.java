package lambdas;

import java.util.Comparator;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class Main {
	public static void main(String[] args) {
		FunctionalInterface fi = new FunctionalInterfaceImpl();
		
		System.out.println(fi.isEven("even"));
		System.out.println(fi.isEven("odd"));
		
		/*
		 * This is called an anonymous class
		 * 	- this is very clunky... lambdas solves this
		 * 
		 * lambdas is a syntax that allows you to implement anonymous classes more cleanly
		 * 
		 */
		FunctionalInterface fc = new FunctionalInterface() {
			
			@Override
			public boolean isEven(String str) {
				
				if(str.length() % 2 == 0) {
					return true;
				}
				return false;
			}
		};
		
		System.out.println(fc.isEven("even"));
		System.out.println(fc.isEven("odd"));
		
		
		/*
		 * A lambda looks like an arrow function in javascript
		 * 	- it was introduced in java 8
		 * 	- you can utilize them to replace anonymous class implementations
		 * 
		 * Built in Functional Interfaces:
		 * 	- Predicate
		 * 	- Comparator
		 *  - Comparable
		 *  - Supplier
		 * 
		 */
		
		//Predicate - write a test
		//Comparator - compares two objects
		//Comparable - passes one object to compare
		//Consumer
		
		
		Consumer<String> consume = str -> System.out.println(new StringBuilder(str).reverse().toString());
		
		
		consume.accept("Kevin");
		consume.accept("Christina");
		/*
		 * FunctionalInterface fl = (str) -> { if(str.length() % 2 == 0 ) return true;
		 * 
		 * return false; };
		 */
		FunctionalInterface fl = str -> str.length() % 2 == 0;
		
		Predicate<String> p = str -> str.length() % 2 == 0; 
		
		System.out.println(p.test("Christina"));
		
		
		System.out.println(fl.isEven("even"));
		System.out.println(fl.isEven("odd"));
		
		
		
		
	}
}
