package lambdas;

public class FunctionalInterfaceImpl implements FunctionalInterface{

	@Override
	public boolean isEven(String str) {
		
		if(str.length() % 2 == 0) {
			return true;
		}
		return false;
	}
	
}
