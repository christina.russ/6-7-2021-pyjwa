package driver;

import java.util.ArrayList;
import java.util.List;

import io.javalin.Javalin;
import models.User;

public class Main {
	public static void main(String[] args) {
		/*
		 * What is Javalin?
		 * 	- Javalin is a server software
		 *  - It allows you to actively listen and wait for HTTP requests
		 *  
		 *  Javalin utilizes something called a handler
		 *  
		 *  Handler is a functional interface. The abstract method that it uses is passed a Context.
		 *  
		 *  Context is how we are going to send and retrieve information from the user
		 *  
		 */
		
		Javalin app = Javalin.create().start(9000);
		
		app.get("/", ctx -> ctx.result("Hello World"));
		
		
		//sending back a response status
		app.get("/response-status", context -> {
			context.status(418);
		});
		
		/*
		 * we can use .result to utilize any response that we would like
		 * as long as we change the contentType
		 */
		app.get("/result", context -> {
			context.contentType("application/json");
			context.result("{\"field1\": 5, \"field2\": [ 5,10,\"hello\"]}");
		});
		
		/*
		 * We can use .html()
		 * Javalin is automatically doing context.contentType("text/html");
		 */
		app.get("/html", context -> {
			context.html("<html><body><h1>Sending Back HTML</h1></body></html>");
		});
		
		
		/*
		 * json
		 * 
		 * If you objects to automatically to converted to json strings, you need to use a dependency call jackson
		 * 
		 * Jackson allows you to convert objects to json strings and visa versa
		 */
		app.get("/json", context -> {
			context.json(new User("Christina", "Russ", 63));
		});
		
		
		/*
		 * lets try a collection of objects. Jackson does this for us too XD
		 */
		
		app.get("/json-list", context -> {
			List<User> users = new ArrayList<>();
			users.add(new User("Christina", "Russ", 63));
			users.add(new User("Kevin", "Childs", 40));
			users.add(new User("Captain", "Murica", 130));
			
			context.json(users);
		});
		
		
		/*
		 * Ways to get information back from the user
		 * 	- queryParams = /user?variableName=value
		 * 	- pathParams = /user/:variableValue
		 * 	- formParams = /user
		 *  - body = /user (in the request body)
		 */
		
		
		/*
		 * query param example
		 */
		app.get("/queryparam", context -> {
			String firstParam = context.queryParam("applejacks");
			String secondParam = context.queryParam("luckycharms");
			
			System.out.println(firstParam);
			System.out.println(secondParam);
			
			context.result("This is what you sent us: " + firstParam+ " " + secondParam);
		});
		
		
		/*
		 * path param example
		 */
		app.get("/pathparam/:ghost", context -> {
			String param = context.pathParam("ghost");
			
			System.out.println("This is what you sent us: " + param);
			context.result("This is what you sent us: " + param);
		});
		
		
		/*
		 * form params example
		 * 
		 * renamed to pictureFram to prove a point that it could be named anything...
		 * 		however convention is context or ctx
		 */
		
		app.post("/formparams", pictureFrame -> {
			String firstParam = pictureFrame.formParam("zebra");
			String secondParam = pictureFrame.formParam("giraffe");
			
			System.out.println(firstParam);
			System.out.println(secondParam);
			
			pictureFrame.result("This is what you sent us: " + firstParam + " " + secondParam);
		});
		
		/*
		 * request body
		 */
		app.post("/request-body", context -> {
			//System.out.println(context.body());
			User user = context.bodyAsClass(User.class);
			
			System.out.println(user);
		});
		
		
		/*
		 * Headers
		 */
		
		app.get("/headers", context -> {
			System.out.println("In header demo");
			
			
			//shows all of the request headers
			System.out.println(context.headerMap());
			
			//you can also send headers back to the user 
			context.header("serverHeader", "icecream");
			context.result("your response now has special headers");
		});
		
		
		/*
		 * What is middleware?
		 * 	middleware allows you to add extra checks to specific endpoints before and after a request
		 * 	
		 */
		//endpoint handler
		app.get("/api/endpointhandler", context -> {
			System.out.println("This is the api endpoint method");
			context.result("endpoint handler");
		});
		
		//endpoint handler
		app.get("/api2/endpointhandler", context -> {
			System.out.println("This is the other api endpoint method");
			context.result("another endpoint handler");
		});
		
		/*
		 * this is going to trigger BEFORE any request
		 */
		app.before("*", context -> {
			System.out.println("THIS GETS HIT EVERYTIME BEFORE");
		});
		
		app.after("*", context -> {
			System.out.println("THIS GETS HIT EVERYTIME AFTER");
		});
		
		/*
		 * can specify routes
		 * 
		 * this can be beneficial to have protected / authenticated routes
		 */
		
		app.before("/api2/*", context -> {
			System.out.println("api2 got hit");
		});
		
		
		////////////////////SESSION DEMO///////////////////////
		app.post("/api/check-session", context -> {
			//sessionAttribute with one argument checks the value of the string given
			User user = context.sessionAttribute("currentUser");
			
			if(user == null) {
				context.result("you are not logged in!");
			}else {
				context.json(user);
			}
			
		});
		
		
		app.post("/api/login", context -> {
			User user = context.bodyAsClass(User.class);
			
			//seessionAttribute with two arguments sets a value to the string
			context.sessionAttribute("currentUser", user);
			
			
			context.result("you are currently logged in!");
		});
		
		
		
		app.post("/api/logout", context -> {
			//set the string to null if you would like to logout.
			context.sessionAttribute("currentUser", null);
			
			context.result("you have been logged out");
		});
		
	}
}
